import com.sun.istack.internal.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MergeSort
    {
        ArrayList<Integer> arrayList;

        public MergeSort(@NotNull ArrayList<Integer> arrayList)
            {
                this.arrayList = arrayList;
            }

        public void sort()
            {
                int lo = 0;
                int hi = arrayList.size();
                sort(lo, hi);
            }


        private void sort(int lo, int hi)
            {
                // lo,hi));
                if (lo + 1 < hi)
                    {
                        int mi = (lo + hi) >> 1;
                        sort(lo, mi);
                        sort(mi, hi);
                        merge(lo, mi, hi);
                    }
            }

        private void merge(int lo, int mi, int hi)
            {
                List<Integer> alb = new ArrayList<>();
                alb.addAll(arrayList.subList(lo, mi));//j
                for (int i = lo, j = 0, k = mi; j < alb.size(); )
                    {
                        int chosen;
                        //arrayListC not ended and the element is lower
                        if (k < hi && arrayList.get(k) < alb.get(j))
                            {
                                chosen = arrayList.get(k++);
                            } else
                            {
                                chosen = alb.get(j++);
                            }
                        arrayList.set(i++, chosen);
                    }
            }
    }