import com.sun.deploy.util.ArrayUtil;
import com.sun.tools.javac.util.ArrayUtils;

import java.util.*;

/**
 * Created by JackPeng(pengjunkun@gmail.com) on 29/08/2017.
 */
public class Start
    {
        static BubbleSort bubbleSort;
        static MergeSort mergeSort;

        public static void main(String[] args)
            {
                bubbleSort = new BubbleSort();
                ArrayList<Integer> testArray1 = new ArrayList<>();
                ArrayList<Integer> testArray2 = new ArrayList<>();
                ArrayList<Integer> testArray3 = new ArrayList<>();

                int i = -1;
                Random random = new Random(3);
                while (++i < 300)
                    {
                        testArray1.add(random.nextInt(100));
                    }
                testArray2 = (ArrayList<Integer>) testArray1.clone();
                testArray3 = (ArrayList<Integer>) testArray1.clone();
                mergeSort=new MergeSort(testArray3);
                System.out.println("ori_same:" + testArray1.equals(testArray2));
                long t1 = Calendar.getInstance().getTimeInMillis();
                System.out.println("t1:" + t1);
                bubbleSort.sortA(testArray1);
                long t2 = Calendar.getInstance().getTimeInMillis();
                System.out.println("t2:" + t2);
                System.out.println("sortA_time:" + (t2 - t1));
                bubbleSort.sortB(testArray2);
                long t3 = Calendar.getInstance().getTimeInMillis();
                System.out.println("sortB_time:" + (t3 - t2));
                mergeSort.sort();
                System.out.println("bubbleSort A:"+checkResult(testArray1));
                System.out.println("bubbleSort B:"+checkResult(testArray2));
                System.out.println("MergeSort:"+checkResult(testArray3));
            }

        public static boolean checkResult(ArrayList<Integer> result)
            {
                for (int i = 0; i < result.size() - 1; i++)
                    {
                        if (result.get(i) > result.get(i + 1)) return false;
                    }
                return true;
            }
    }