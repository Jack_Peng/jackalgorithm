import java.lang.System;
import java.util.ArrayList;

public class BubbleSort
{
	//caution: [lo,hi)
	public void sortA(ArrayList<Integer> S)
	{
		int lo = 0;
		int hi = S.size();
		while (!bubbleA(S, lo, hi--))
			;
	}

	private boolean bubbleA(ArrayList<Integer> S, int lo, int hi)
	{
		boolean sorted = true;
		while (++lo < hi)
			if (S.get(lo - 1) > S.get(lo))
			{
				sorted = false;
				swap(S, lo - 1, lo);
			}
		return sorted;
	}

	private void swap(ArrayList<Integer> S, int a, int b)
	{
		int tmp = S.get(a);
		S.set(a, S.get(b));
		S.set(b, tmp);
	}

	public void sortB(ArrayList<Integer> S)
	{
		int lo = 0;
		int hi = S.size();
		while (lo + 1 < (hi = bubbleB(S, lo, hi)))
			;
	}

	//sort once and return the new high index
	private int bubbleB(ArrayList<Integer> S, int lo, int hi)
	{
		int last = lo;
		while (++lo < hi)
			if (S.get(lo - 1) > S.get(lo))
			{
				swap(S, lo - 1, lo);
				last = lo;
			}
		return last;
	}
}